
import 'package:get/get.dart';

class DetailScreenController extends GetxController {
  String contactString = '';

  @override
  void onInit() {
    super.onInit();
    var arguments = Get.arguments;
    if(arguments is String) {
      contactString = arguments;
    }
  }
}