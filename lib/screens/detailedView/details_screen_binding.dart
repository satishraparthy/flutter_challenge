import 'package:flutter_challenge/screens/detailedView/detail_screen_controller.dart';
import 'package:flutter_challenge/screens/groupView/group_view_controller.dart';
import 'package:get/get.dart';

class DetailScreenBinding extends Bindings {

  @override
  void dependencies() {
    Get.lazyPut(() => DetailScreenController());
  }
}
