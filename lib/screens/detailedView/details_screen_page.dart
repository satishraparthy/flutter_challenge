import 'package:flutter/material.dart';
import 'package:flutter_challenge/screens/detailedView/detail_screen_controller.dart';
import 'package:flutter_challenge/utils/styles.dart';
import 'package:get/get.dart';

class DetailsScreenPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Detailed View'),
      ),
      body: Center(
        child: GetBuilder<DetailScreenController>(
            builder: (controller) => Text(
                  controller.contactString,
                  style: boldTextStyle(),
                )),
      ),
    );
  }
}
