import 'package:flutter/material.dart';
import 'package:flutter_challenge/data/models/grouped_contacts.dart';
import 'package:flutter_challenge/data/providers/contacts_provider.dart';
import 'package:get/get.dart';

class GroupViewController extends GetxController {
  TextEditingController searchController = new TextEditingController();
  bool isLoading = false;

  var _searchedContacts = <String>[];
  List<String> _contacts;
  List<GroupedContact> lstGroupedContacts = [];
  ContactsProvider contactsProvider;

  @override
  void onInit() {
    super.onInit();
    contactsProvider = Get.find<ContactsProvider>();
    _contacts = contactsProvider.getContacts();
    lstGroupedContacts =
        contactsProvider.getGroupedContacts(lstContacts: _contacts);
  }

  //Search Filter
  getSearchResult(String searchName) {
    isLoading = true;
    _searchedContacts.clear();
    lstGroupedContacts.clear();
    update();
    for (int i = 0; i < _contacts.length; i++) {
      String data = _contacts[i];
      if (data.toLowerCase().contains(searchName.toLowerCase())) {
        _searchedContacts.add(data);
      }
    }
    lstGroupedContacts =
        contactsProvider.getGroupedContacts(lstContacts: _searchedContacts);
    isLoading = false;
    update();
  }
}
