import 'package:flutter_challenge/screens/groupView/group_view_controller.dart';
import 'package:get/get.dart';

class GroupViewBinding extends Bindings {

  @override
  void dependencies() {
    Get.lazyPut(() => GroupViewController());
  }
}
