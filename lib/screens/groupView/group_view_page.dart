import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_challenge/routes/app_routes.dart';
import 'package:flutter_challenge/screens/detailedView/details_screen_page.dart';
import 'package:flutter_challenge/screens/groupView/group_view_controller.dart';
import 'package:flutter_challenge/utils/constants/color_constant.dart';
import 'package:flutter_challenge/utils/constants/string_constant.dart';
import 'package:flutter_challenge/utils/styles.dart';
import 'package:get/get.dart';

class GroupViewPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: [
          SliverAppBar(
            elevation: 2,
            expandedHeight: 200,
            floating: true,
            pinned: true,
            titleSpacing: 0,
            actionsIconTheme: IconThemeData(opacity: 0.0),
            title: Container(
              height: 35,
              child: Padding(
                padding: const EdgeInsets.fromLTRB(16, 8, 16, 0),
                child: Text(
                  appNameText,
                  style: TextStyle(
                      color: whiteColor,
                      fontSize: 20,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ),
            bottom: PreferredSize(
              preferredSize: const Size.fromHeight(kToolbarHeight + 60),
              child: Padding(
                padding: EdgeInsets.only(left: 20),
                child: GetBuilder<GroupViewController>(
                  builder: (controller) => TextField(
                    controller: controller.searchController,
                    onChanged: controller.getSearchResult,
                    cursorColor: whiteColor,
                    autofocus: false,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                      fontSize: 35,
                    ),
                    decoration: InputDecoration(
                      suffixIcon: IconButton(
                        icon: Icon(Icons.close, color: Colors.white),
                        onPressed: () {
                          controller.searchController.clear();
                          controller.getSearchResult('');
                          FocusScope.of(context).unfocus();
                        },
                      ),
                      border: InputBorder.none,
                      hintText: searchText,
                      hintStyle: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 35,
                        color: textSearchColor,
                      ),
                    ),
                  ),
                ),
              ),
            ),
            actions: [],
          ),
          GetBuilder<GroupViewController>(
            builder: (controller) => controller.isLoading
                ? Center(
                    child: CircularProgressIndicator(),
                  )
                : SliverList(
                    delegate: SliverChildListDelegate(
                      [
                        Container(
                          padding: EdgeInsets.symmetric(horizontal: 16),
                          child: ListView.separated(
                            shrinkWrap: true,
                            physics: ClampingScrollPhysics(),
                            itemCount: controller.lstGroupedContacts.length,
                            itemBuilder: (context, index) {
                              var groupedContact =
                                  controller.lstGroupedContacts[index];
                              var strGroupingCharacter =
                                  groupedContact.strGroupingCharacter;
                              return Row(
                                children: [
                                  CircleAvatar(
                                    backgroundColor: primaryColor,
                                    radius: 25,
                                    child: Text(
                                      strGroupingCharacter,
                                      style: TextStyle(color: whiteColor),
                                    ),
                                  ),
                                  SizedBox(
                                    width: 16,
                                  ),
                                  Expanded(
                                      child: Column(
                                    children: groupedContact.lstContacts
                                        .map((contactString) => ListTile(
                                              contentPadding:
                                                  EdgeInsets.symmetric(
                                                      vertical: 5,
                                                      horizontal: 5),
                                              title: Text(
                                                contactString,
                                                style: boldTextStyle(size: 14),
                                              ),
                                              onTap: () {
                                                Get.toNamed(
                                                    AppRoutes.detailedView,
                                                    arguments: contactString);
                                              },
                                            ))
                                        .toList(),
                                  ))
                                ],
                              );
                            },
                            separatorBuilder: (context, index) {
                              return Divider();
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
          )
        ],
      ),
      persistentFooterButtons: [
        Container(
          height: 40,
          padding: EdgeInsets.only(left: 15, right: 15),
          width: Get.width,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [],
          ),
        )
      ],
    );
  }
}
