import 'package:flutter_challenge/data/models/grouped_contacts.dart';

class ContactsProvider {
  final _contacts = <String>[
    'Marco Franco',
    'Raul Alday',
    'Jessica Alba',
    'Roger Waters',
    'Darth Vader',
    'Homer Simpson',
    'Bill Gates',
    'Elon Musk',
    'Enrique Peña',
    'Angeles Rodriguez',
    'Monica Alvarado',
    'Estrella Fugaz',
    'Juana Lopez',
  ];

  List<String> getContacts() {
    List<String> lstContacts = [];
    lstContacts.addAll(_contacts);
    lstContacts.sort((a, b) => a.toLowerCase().compareTo(b.toLowerCase()));
    return lstContacts;
  }

  List<GroupedContact> getGroupedContacts({List<String> lstContacts}) {
    List<GroupedContact> lstGroupedContacts = [];

    lstContacts.forEach((strContact) {
      var firstChar = strContact[0];
      try {
        var groupedContactFromList = lstGroupedContacts.firstWhere(
            (groupedContact) =>
                groupedContact.strGroupingCharacter == firstChar);
        groupedContactFromList.lstContacts.add(strContact);
      } catch (e) {
        lstGroupedContacts.add(GroupedContact()
          ..strGroupingCharacter = firstChar
          ..lstContacts.add(strContact));
      }
    });

    return lstGroupedContacts;
  }
}
