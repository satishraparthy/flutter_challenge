class GroupedContact {

  List<String> lstContacts = [];
  String strGroupingCharacter;

  @override
  String toString() {
    return 'GroupedContacts{lstContacts: $lstContacts, strGroupingCharacter: $strGroupingCharacter}';
  }
}