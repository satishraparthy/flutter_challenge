import 'package:flutter_challenge/screens/detailedView/details_screen_binding.dart';
import 'package:flutter_challenge/screens/detailedView/details_screen_page.dart';
import 'package:flutter_challenge/screens/groupView/group_view_binding.dart';
import 'package:flutter_challenge/screens/groupView/group_view_page.dart';
import 'package:get/get.dart';

import 'app_routes.dart';

class AppPages {
  static final List<GetPage> pages = [
    getPageWithTransition(
      name: AppRoutes.groupView,
      page: () => GroupViewPage(),
      binding: GroupViewBinding(),
    ),
    getPageWithTransition(
      name: AppRoutes.detailedView,
      page: () => DetailsScreenPage(),
      binding: DetailScreenBinding(),
    ),
  ];

  static getPageWithTransition({String name, GetPageBuilder page, Bindings binding}) =>
      GetPage(
        name: name,
        page: page,
        binding: binding,
        transition: Transition.rightToLeft,
        transitionDuration: Duration(milliseconds: 777),
      );
}
