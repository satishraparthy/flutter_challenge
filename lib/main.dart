import 'package:flutter/material.dart';
import 'package:flutter_challenge/screens/groupView/group_view_binding.dart';
import 'package:flutter_challenge/screens/groupView/group_view_page.dart';
import 'package:get/get.dart';

import 'routes/app_pages.dart';
import 'utils/constants/color_constant.dart';
import 'utils/dependency_injection.dart';

void main() {
  DependencyInjection.init();

  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        scaffoldBackgroundColor: whiteColor
      ),
      home: GroupViewPage(),
      initialBinding: GroupViewBinding(),
      getPages: AppPages.pages,
    );
  }
}