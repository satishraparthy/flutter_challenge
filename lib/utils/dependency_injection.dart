import 'package:flutter_challenge/data/providers/contacts_provider.dart';
import 'package:get/get.dart';

/// Initialize classes that handle data/ business logic
class DependencyInjection {
  /// Initialize app wide Repositories, Services, Utilities here
  static void init() {
    // Providers
    Get.put(ContactsProvider(), permanent: true);
  }
}
